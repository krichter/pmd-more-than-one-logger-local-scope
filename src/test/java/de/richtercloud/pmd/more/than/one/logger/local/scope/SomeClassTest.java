package de.richtercloud.pmd.more.than.one.logger.local.scope;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.mockito.Mockito.mock;

public class SomeClassTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SomeClassTest.class);

    @Test
    public void someTest() {
        LOGGER.debug("run test 'someTest'");
        Logger loggerMock = mock(Logger.class);
        SomeClass instance = new SomeClass();
        instance.doSomethingWithALogger(loggerMock);
    }
}
